import json
from datetime import datetime, timedelta, timezone

import pytest

pytestmark = pytest.mark.asyncio


# connection parameter is required to create the table.
async def test_index_returns_200(req, connection):
    response = await req('/positions/')
    assert response.status == b'200 OK'
    assert response.body == '{"data":[]}'


# connection parameter is required to create the table.
async def test_index_returns_json(req, connection):
    response = await req('/positions/')
    assert json.loads(response.body) == {"data": []}


async def test_index_returns_drones(factory, req):
    now = datetime.now(timezone.utc)
    await factory(coordinates=[1, 2, 3], time=now, registration='test_foo')
    response = await req('/positions/')
    data = json.loads(response.body)['data']
    assert data[0]['coordinates'] == [1.0, 2.0, 3.0]
    assert data[0]['time'] == int(now.timestamp())
    assert data[0]['registration'] == 'test_foo'


async def test_old_drones_are_not_retrieved(factory, req):
    now = datetime.now(timezone.utc) - timedelta(seconds=31)
    await factory(coordinates=[1, 2, 3], time=now, registration='test_foo')
    response = await req('/positions/')
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_fresh_drones_are_retrieved(factory, req):
    now = datetime.now(timezone.utc) - timedelta(seconds=29)
    await factory(coordinates=[1, 2, 3], time=now, registration='test_foo')
    response = await req('/positions/')
    data = json.loads(response.body)['data']
    assert len(data) == 1


async def test_only_one_position_per_drone(factory, req):
    now = datetime.now(timezone.utc)
    previous = now - timedelta(seconds=29)
    await factory(coordinates=[1, 2, 3], time=previous, registration='foo')
    await factory(coordinates=[4, 5, 6], time=now, registration='foo')
    response = await req('/positions/')
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['coordinates'] == [4.0, 5.0, 6.0]


async def test_reports_invalid_hash(req):
    response = await req('/report/foo')
    assert response.status == b'400 Bad Request'
    assert response.body == '{"error":"Invalid report hash"}'


async def test_reports_unknown_hash(req):
    response = await req('/report/{}'.format('*' * 32))
    assert response.status == b'404 Not Found'
    assert response.body == '{"error":"Unknown report hash"}'


async def test_reports_with_error(req, report):
    key = '*' * 32
    await report(key=key, errors={'foo': 'bar'})
    response = await req('/report/{}'.format(key))
    assert response.status == b'200 OK'
    payload = json.loads(response.body)
    assert payload['error'] == '{"foo": "bar"}'
