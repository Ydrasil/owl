import json
from datetime import datetime, timezone

import asyncpg
import pytest
from postgis import Point
from postgis.asyncpg import register
from server import app as myapp
from server import DB_CONFIG

WSG84 = 4326


@pytest.yield_fixture
async def connection():
    conn = await asyncpg.connect(**DB_CONFIG)
    await register(conn)
    # TODO: find a way to factorize with squirrel?
    await conn.execute(
        'CREATE TABLE IF NOT EXISTS data ('
        '    id serial PRIMARY KEY,'
        '    coordinates geography(PointZ) NOT NULL,'
        '    time timestamp with time zone NOT NULL,'
        '    registration varchar(128) NOT NULL'
        ');')
    await conn.execute(
        'CREATE TABLE IF NOT EXISTS reports ('
        '    key varchar(32) PRIMARY KEY,'
        '    data bytea NOT NULL,'
        '    errors jsonb NOT NULL,'
        '    time timestamp with time zone NOT NULL'
        ');')
    yield conn
    await conn.execute('TRUNCATE data;')
    await conn.execute('TRUNCATE reports;')
    await conn.close()


@pytest.yield_fixture
async def factory(connection):
    async def insert_payload(**payload):
        default = {
            'coordinates': [1, 2, 3],
            'time': datetime.now(timezone.utc),
            'registration': 'test_foo'
        }
        default.update(payload)
        point = Point(*default['coordinates'], srid=WSG84)
        time_ = default['time']
        registration = default['registration']
        await connection.execute(
            'INSERT INTO "data" (coordinates, time, registration) '
            'VALUES ($1, $2, $3)', point, time_, registration)
    yield insert_payload


@pytest.yield_fixture
async def report(connection):
    async def insert_report(key, errors):
        errors = json.dumps(errors)
        await connection.execute(
            'INSERT INTO "reports" (key, data, errors, time) '
            'VALUES ($1, $2, $3, $4)',
            key, b'', errors, datetime.now(timezone.utc))
    yield insert_report


@pytest.fixture
def app():
    return myapp
