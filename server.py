import asyncio
import os

from asyncpg import create_pool
from postgis.asyncpg import register
from roll import Roll
from roll.extensions import cors
import ujson as json
import uvloop

DB_CONFIG = {
    'database': os.environ.get('POSTGRES_DB', 'squirrel'),
    'host': os.environ.get('POSTGRES_HOST', None),
    'user': os.environ.get('POSTGRES_USER', None),
    'password': os.environ.get('POSTGRES_PASSWORD', None),
}


asyncio.set_event_loop(uvloop.new_event_loop())
app = Roll()
cors(app)


def jsonify(records):
    """
    Parse asyncpg record response into JSON format
    """
    return [dict(r.items()) for r in records]


def json_response(code, **kwargs):
    return (json.dumps(kwargs), code, {'Content-Type': 'application/json'})


@app.listen('startup')
async def register_db():
    app.pool = await create_pool(**DB_CONFIG, max_size=100, loop=app.loop,
                                 init=register)


@app.route('/positions/')
async def positions(request):
    async with app.pool.acquire() as connection:
        # Always compare dates in the same timezone, either in the
        # system timezone or in UTC.
        results = await connection.fetch(
            "SELECT time, registration, coordinates FROM ("
            "   SELECT time AT TIME ZONE 'UTC' AS time, "
            "          registration, coordinates, "
            "          row_number() OVER (PARTITION BY registration "
            "                             ORDER BY time DESC) "
            "   FROM data "
            "   WHERE time >= now() - '30 seconds'::interval) as tmp "
            "WHERE row_number=1 "
            "ORDER BY time DESC")
        return json_response(200, data=jsonify(results))


@app.route('/report/:error_hash')
async def report(request, error_hash):
    if len(error_hash) != 32:
        return json_response(400, error='Invalid report hash')
    async with app.pool.acquire() as connection:
        report = await connection.fetchrow(
            'SELECT errors, data FROM reports WHERE key=$1', error_hash)
        if not report:
            return json_response(404, error='Unknown report hash')
        return json_response(
            200, error=report['errors'],
            data=bytes(report['data']).decode(errors='ignore'))

if __name__ == '__main__':
    app.serve(host='127.0.0.1', port=8080)
